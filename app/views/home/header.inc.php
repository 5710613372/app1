<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BakeryCake</title>
    <style>
        html{
            background-color: azure;
        }
        .header{
            background-image: url("1.jpg");
            flex-basis: 100%;
        }
        .container {
            display: flex;
            background-color: lemonchiffon;
            width: 990px;
            flex-flow: row wrap;
            margin: auto;
        }
        .footer{
            alignment: center;
            background-color: gray;
            margin: auto;
            flex-basis: 100%;
        }
        div.header h1{
            margin-bottom: 0;
            padding: 40px;
            padding-top: 30px;
            padding-bottom: 30px;
            text-align:center  ;
            letter-spacing:0.3em;
        }
        .side-site{
            flex:1;
            padding-right: 40px;
            padding-top: 40px;
            font-family: 'Dosis', sans-serif;
        }
        .main{
            flex: 2;
            padding: 40px;
            font-family: 'Dosis', sans-serif;
        }
        aside img{
            width:150px;
            height: 100px;
        }
        .header a{
            border:3px solid burlywood;
            background: lightgoldenrodyellow;
            text-decoration: none;
        }

    </style>
</head>
<div class="container">
    <div class="header">
        <div><h1>Cake Bakery</h1></div>
        <a href="index.inc.php">Home</a>
        <a href="list.inc.php">List</a>
        <a href="login.inc.php">Log in</a>
        <a href="register.inc.php">Register</a>
        <a href="contact.inc.php">Contact</a>
    </div>
