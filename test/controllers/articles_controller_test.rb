require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  test "should get page1" do
    get :page1
    assert_response :success
  end

  test "should get page2" do
    get :page2
    assert_response :success
  end

  test "should get page3" do
    get :page3
    assert_response :success
  end

  test "should get page4" do
    get :page4
    assert_response :success
  end

  test "should get page5" do
    get :page5
    assert_response :success
  end

  test "should get page6" do
    get :page6
    assert_response :success
  end

  test "should get page7" do
    get :page7
    assert_response :success
  end

  test "should get page8" do
    get :page8
    assert_response :success
  end

end
