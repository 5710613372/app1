Feature: Order
Scenario: A customer want to order a cake
  Given A customer has a valid Credit or Debit card
  When he/she put the cake to the list to buy in list page
  Then he/she has all the cake that he/she want to buy
  When he/she buy the cake in buy page
  And he/she has enough money to buy the cake 
  Then the cake will be delivered to him/her correctly 
    